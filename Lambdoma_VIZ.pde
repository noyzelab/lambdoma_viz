// Visualisation of Barbara Hero's Lissajous Figure representation of the Lambdoma Matrix.
// Clockwise from top left text indicates : frequency, ratio, wavelength & angle.
// Image is exported as a 900x900 jpeg file.
// 
// Code : David Burraston. Originally coded in 2013, slightly revised March 2020 
//
// Ref: 
// Lambdoma Matrix and Harmonic Intervals: The Physiological and Psychological Effects on Human Adaptation from Combining Math and Music, Engineering in Medicine and Biology Magazine, Vol. 18 Number 2, March/April 1999, IEEE.
// Available at http://www.lambdoma.com/Lambdoma-Music-Barbara-Hero/barbara-heros-chapters-in-other-authors-books/#intervals
// 

int spacer;
float fundamental;
float freq;
float wavelength;
int row,col;
PFont f;

void setup() {
  
  size(900, 900);
  spacer = 105;  
  f = createFont("Courier New Bold", 24);
  textFont(f);
  strokeWeight(1.15); 
  stroke(0,128,128);
  translate(spacer/10,spacer/10);
  background(0);
  
  row = 8;
  col = 8;
  fundamental = 33;

    for (int y = 0; y < row; y++) 
    {
      for (int x = 0; x < col; x++) 
      { 
        fill(0);
        rect(x* spacer, y* spacer, spacer, spacer);
        fill(255);
        textFont(f, 12);
        freq = fundamental*(x+1.0)/(y+1.0); 
        wavelength = 1130.0/freq;
        text(freq, x* spacer,  y* spacer+12);     
        text(x+1, x* spacer+75, y* spacer+12);
        text(":", x* spacer+82, y* spacer+12);
        text(y+1, x* spacer+85, y* spacer+12);
        text(atan((x+1.0)/(y+1.0))/0.0174533, x* spacer,  spacer-3+(y* spacer));             
        text(wavelength, x* spacer+50, spacer-3+(y* spacer));
        pushMatrix();
        translate(x* spacer+spacer/2, y* spacer+spacer/2);
        rotate(PI/2.0);
     
        for (int i = 0; i < 3000; i++) 
        {
          point((spacer/2.85)*sin(i), (spacer/2.85)*cos((y+1.0)/(x+1.0)*i)); 
        }
        popMatrix();      
    }
   }
   fill(0,128,128);
   textFont(f, 32);
   text("Lambdoma Matrix | ", 10, 875);
   text("Fundamental : ", 350, 875);
   text(fundamental, 600, 875);
   text("Hz", 750, 875);
   save("lambdoma-pic.jpg");
}

void draw() {
}

 