# Lambdoma_VIZ

Visualisation of Barbara Hero's Lissajous Figure representation of the Lambdoma Matrix.
Image is exported as a 900x900 jpeg file. Program written in Processing language.
www.processing.org

if u find this prog useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or get in touch via noyzelab [at] gmail [dot] com
thanks, dave